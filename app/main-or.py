from flask import Flask, render_template, request
import pandas as pd
from sklearn.externals import joblib
import numpy as np

app = Flask(__name__)
model = joblib.load('litemodel.sav')

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/', methods=['POST'])
def get():
    input = dict(request.form)

    accidentindex = input['accidentindex'][0]
    age_of_vehicle = input['age_of_vehicle'][0]
    vehicle_type = input['vehicle_type'][0]
    age_of_driver = input['age_of_driver'][0]
    day = input['day'][0]
    weather = input['weather'][0]
    light = input['light'][0]
    roadsc = input['roadsc'][0]
    gender = input['gender'][0]
    speedl = input['speedl'][0]

    data = np.array([1, age_of_driver, vehicle_type, age_of_vehicle, 4266, day, weather, roadsc, light, gender, speedl])
    data = data.astype(float)
    data = data.reshape(1, -1)

    x = np.array([1, 3.73, 3, 0.69, 125, 4, 1, 1, 1, 1, 30]).reshape(1, -1)
    
    try: result = model.predict(data)
    except Exception as e: result = str(e)

    return str(result[0])

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=4000)
